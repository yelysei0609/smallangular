/* eslint-disable array-callback-return */
/* eslint-disable global-require */
const express = require('express');
const fileUpload = require('express-fileupload');
const fs = require('fs');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');

const users = [
  {
    firstName: 'Alexey',
    lastName: 'Alexeyevich',
    email: 'alexey@gmail.com',
    password: 'alexey111'
  },
  {
    firstName: 'Vasiliy',
    lastName: 'Vasilievich',
    email: 'vasiliy@gmail.com',
    password: 'vasiliy111'
  },
  {
    firstName: 'Petro',
    lastName: 'Shokoladniy',
    email: 'petro@gmail.com',
    password: 'petro111'
  },
  {
    firstName: 'asd',
    lastName: 'Shokasdadniy',
    email: 'a@a.a',
    password: '1'
  }
];

if (process.env.NODE_ENV === 'production') {
  app.use('/form', express.static(path.join(__dirname, '/public/index.html')));

  app.use(
    '/app.bundle.js',
    express.static(path.join(__dirname, '/public/app.bundle.js'))
  );
} else {
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const config = require('./webpack.config.js');
  config.entry.app.unshift(
    'webpack-hot-middleware/client?reload=true&timeout=1000'
  );
  config.plugins.push(new webpack.HotModuleReplacementPlugin());
  const compiler = webpack(config);
  app.use(
    webpackDevMiddleware(compiler, {
      publicPath: config.output.publicPath
    })
  );

  app.use(webpackHotMiddleware(compiler));
}

app.use('/files', express.static(path.join(__dirname, '/uploads')));
app.use(
  '/favicon.ico',
  express.static(path.join(__dirname, '/assets/favicon.ico'))
);

app.use(fileUpload());
app.use(bodyParser.json());

app.post('/ping', function(req, res) {
  res.send('pong');
});

app.get('/list', (req, res) => {
  fs.readdir('./uploads', (err, files) => {
    if (err) {
      return res.send(err);
    }
    res.send(files.filter(file => file !== '.gitkeep'));
  });
});

app.post('/login', (req, res) => {
  const { email, password } = req.body;
  let newUser = null;
  users.forEach(user => {
    if (user.email === email && user.password === password) {
      newUser = { firstName: user.firstName, lastName: user.lastName };
    }
  });

  if (newUser) {
    res.status(200).send(newUser);
  } else {
    res.status(401).send('Wrong email or password!');
  }
});

app.post('/upload', function(req, res) {
  let sampleFile = null;
  let uploadPath = null;

  if (Object.keys(req.files).length === 0) {
    res.status(400).send('No files were uploaded.');
    return;
  }

  sampleFile = req.files.sampleFile; // eslint-disable-line

  uploadPath = path.join(__dirname, '/uploads/', sampleFile.name);

  sampleFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send(err);
    }

    res.send({ name: sampleFile.name });
  });
});

app.listen(8000, function() {
  console.log('Express server listening on port 8000'); // eslint-disable-line
});
