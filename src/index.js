/* eslint-disable no-undef */
import { filesListModule, downloadFormModule } from '@components';
import './style.scss';

const app = angular.createApp('myApp');
filesListModule(app);
downloadFormModule(app);
app.bootstrap(document.querySelector('body'));
