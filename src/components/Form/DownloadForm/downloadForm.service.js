function downloadFormService($http, $scope) {
  'inject';

  const data = [];

  function DownloadFile() {
    this.getFile = fileName => {
      $http.get(`/files/${fileName}`, {
        responseType: 'blob'
      })
        .then(res => res.blob())
        .then(res => {
          data.push(res);
          $scope.$applyAsync();
        });
      return data;
    };
  }
  return new DownloadFile();
}

export default downloadFormService;