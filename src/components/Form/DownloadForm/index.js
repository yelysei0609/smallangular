import downloadForm from './downloadForm.component';
import downloadFormController from './downloadForm.controller';
import downloadFormService from './downloadForm.service';

const downloadFormModule = app => {
  app.component(downloadForm);
  app.controller(downloadFormController);
  app.service(downloadFormService);
};


export default downloadFormModule;