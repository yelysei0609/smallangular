import template from './template.html';

const downloadForm = () => ({
  link: ($scope, el, attrs) => {
    const value = el.localName;
    el.innerHTML = template;
    const form = el.querySelector('.form');
    const input = el.querySelector('.downloadInput');
    form.addEventListener('submit', e => {
      // $scope.downloadGetFile()
    });
    const addNodes = compile => {
      const nodes = el.querySelectorAll('*');
      nodes.forEach(compile);
    };

    const setInputValue = () => {
      if ($scope.selectedItem) {
        input.value = $scope.selectedItem;
      }
    };

    $scope.$watch($scope.selectedItem, setInputValue);
  },
  controller: 'download-form-controller'
});

export default downloadForm;