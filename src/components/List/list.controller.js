const filesListController = ($scope, filesListService) => {
  'inject';
  $scope.listData = filesListService.getData();
  $scope.itemClicked = e => {
    $scope.selectedItem = e.target.textContent;
    $scope.$apply();
  };
};

export default filesListController;