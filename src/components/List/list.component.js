import template from './template.html';

const filesList = () => ({
  link: ($scope, el, attrs) => {
    const value = el.localName;
    el.innerHTML = template;

    const loadList = compile => {
      const nodes = el.querySelectorAll('*');
      nodes.forEach(compile);
    };

    $scope.$watch(value, loadList);
  },
  controller: 'files-list-controller'
});

export default filesList;