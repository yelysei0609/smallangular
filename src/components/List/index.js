import filesList from './list.component';
import filesListController from './list.controller';
import filesListService from './list.service';

const filesListModule = app => {
  app.component(filesList);
  app.controller(filesListController);
  app.service(filesListService);
};


export default filesListModule;