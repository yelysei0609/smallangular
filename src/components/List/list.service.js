function filesListService($http, $scope) {
  'inject';

  const data = [];

  function ListLoad() {
    this.getData = () => {
      $http.get('/list')
        .then(res => res.json())
        .then(res => {
          data.push(...res);
          $scope.$applyAsync();
        });
      return data;
    };
  }
  return new ListLoad();
}

export default filesListService;