const deleteNodes = parent => {
  const nodes = parent.querySelectorAll('[repeated-element]');
  nodes.forEach(node => node.remove());
};

export default deleteNodes;