const injection = fn => {
  const fnString = fn.toString();
  const inject = fnString.split(/\n/)[1];

  if (!inject.includes('inject')) {
    return fn;
  }

  const injectionsArr = fnString.match(/\(([^)]+)\)/)[1].split(',').map(fnName => fnName.trim());
  fn.injections = injectionsArr;
  return fn;
};

export default injection;