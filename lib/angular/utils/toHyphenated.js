const toHyphenated = name => {
  const upperToHyphenLower = match => `-${match.toLowerCase()}`;

  return name.replace(/[A-Z]/g, upperToHyphenLower);
};

export default toHyphenated;