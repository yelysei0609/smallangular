/* eslint-disable no-eval */
import * as defaultServices from './services';
import * as defaultDirectives from '@libDirectives';
import { toHyphenated, injection, injectionsSearch } from '@libUtils';
(function () {
  function SmallAngular() {
    const $rootScope = window;

    const directives = {};
    const watchers = [];
    const components = {};
    const controllers = {};
    const services = {};


    function directive(fn) {
      const fnName = toHyphenated(fn.name);

      if (directives[fnName]) {
        return directives[fnName].push(fn);
      }
      directives[fnName] = [fn];
      return this;
    }

    function component(fn) {
      components[toHyphenated(fn.name)] = fn;
      return this;
    }

    function controller(fn) {
      controllers[toHyphenated(fn.name)] = injection(fn);
      return this;
    }

    function service(fn) {
      injection(fn);
      let params = null;

      if (fn.injections) {
        params = injectionsSearch($rootScope, fn.injections, services);
      }
      services[fn.name] = fn(...params);
      return this;
    }


    function initBaseDirectives(fns) {
      Object.values(fns).forEach(directive);
    }

    function initBaseServices(fns) {
      Object.values(fns).forEach(service);
    }

    function createApp(name) {
      return this;
    }

    const compile = node => {
      const nodeAttributes = node.getAttributeNames();
      const filtredAttrs = nodeAttributes.filter(attr => !attr.includes('ng-'));

      if (components[node.localName]) {
        const component = components[node.localName]();
        const controller = controllers[component.controller];
        let params = null;

        if (controller.injections) {
          params = injectionsSearch($rootScope, controller.injections, services);
        }
        controller(...params);

        component.link($rootScope, node, filtredAttrs);

        node.querySelectorAll('*').forEach(compile);
      }

      if (nodeAttributes.includes('ng-if')) {
        const result = directives['ng-if'][0]().link($rootScope, node, filtredAttrs);

        if (!result) {
          return;
        }
      }

      nodeAttributes.forEach(attr => {
        if (directives[attr]) {
          directives[attr].forEach(fn => fn().link($rootScope, node, filtredAttrs));
        }
      });
    };

    $rootScope.$apply = () => {
      watchers.forEach(fn => fn(compile));
    };

    $rootScope.$watch = (name, fn) => {
      watchers.push(fn);
    };

    $rootScope.$applyAsync = () => {
      setTimeout($rootScope.$apply);
    };


    this.bootstrap = node => {
      if (!node) {
        return this;
      }

      const elements = node.querySelectorAll('*');
      compile(node);
      elements.forEach(compile);
      // $rootScope.$apply();
    };

    this.directive = directive;
    this.component = component;
    this.$watchers = watchers;
    this.$components = components;
    this.controller = controller;
    this.service = service;
    this.createApp = createApp;
    initBaseServices(defaultServices);
    initBaseDirectives(defaultDirectives);
    this.bootstrap();
  }

  window.angular = new SmallAngular();
}());
