export default function $http($rootScope) {
  'inject';

  function FetchApi() {
    this.get = (url, config) => fetch(url, {
      ...config,
      method: 'GET'
    });
  }

  return new FetchApi();
}
