const ngShow = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-show');
    const show = () => (el.style.display = eval(value) ? '' : 'none');
    show();
    $scope.$watch(value, show);
  }
});

export default ngShow;