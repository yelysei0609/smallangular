const ngModel = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-model');
    const model = () => (el.value = $scope[value]);
    el.oninput = () => {
      $scope[value] = el.value;
      $scope.$apply();
    };
    model();
    $scope.$watch(value, model);
  }

});

export default ngModel;