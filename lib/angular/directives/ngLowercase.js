const ngLowercase = () => ({
  link: ($scope, el, attrs) => {
    el.style.textTransform = 'lowerCase';
  }

});

export default ngLowercase;