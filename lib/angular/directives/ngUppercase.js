const ngUppercase = () => ({
  link: ($scope, el, attrs) => {
    el.style.textTransform = 'uppercase';
  }
});

export default ngUppercase;