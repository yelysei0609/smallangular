const ngIf = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-if');
    const ifFn = () => (el.style.display = eval(value) ? '' : 'none');
    ifFn();
    $scope.$watch(value, ifFn);
    return Boolean(eval(value));
  }
});

export default ngIf;