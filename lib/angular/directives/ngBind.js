const ngBind = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-bind');
    const bind = () => (el.textContent = $scope[value]);
    bind();
    $scope.$watch(value, bind);
  }

});

export default ngBind;