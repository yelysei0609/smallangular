const ngHide = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-hide');
    const hide = () => (el.style.display = eval(value) ? 'none' : '');
    hide();
    $scope.$watch(value, hide);
  }
});

export default ngHide;