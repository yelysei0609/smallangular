const ngInit = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-init');
    eval(value);
  }
});

export default ngInit;