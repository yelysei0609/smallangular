const ngClick = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-click');
    el.addEventListener('click', () => eval(value));
  }
});

export default ngClick;