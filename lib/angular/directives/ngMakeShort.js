const ngMakeShort = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-make-short');
    const makeShort = () =>
      (el.textContent = `${el.textContent.slice(0, value)}...`);
    makeShort();
    $scope.$watch(value, makeShort);
  }

});

export default ngMakeShort;