import { deleteNodes } from '@libUtils';

const ngRepeat = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-repeat').split(' ')[2];
    const parent = el.parentNode;

    const repeatNodes = compile => {
      deleteNodes(parent);
      const fragment = document.createDocumentFragment();

      [...$scope[value]].forEach(letter => {
        const newEl = el.cloneNode(true);
        newEl.removeAttribute('ng-repeat');
        newEl.setAttribute('repeated-element', true);
        newEl.style.display = '';
        newEl.textContent = el.textContent.replace(/{{(.+)\}}/, letter);

        if (typeof compile === 'function') {
          compile(newEl);
        }
        fragment.appendChild(newEl);
      });
      parent.innerHTML = '';
      parent.appendChild(fragment);
    };
    repeatNodes();
    $scope.$watch(value, repeatNodes);
    $scope.$apply();
  }

});

export default ngRepeat;