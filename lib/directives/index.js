/* eslint-disable no-eval */
import { deleteNodes } from '@utils';

const ngIf = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-if');
    const ifFn = () => (el.style.display = eval(value) ? '' : 'none');
    $scope.$watch(value, ifFn);
    ifFn();
    return Boolean(eval(value));
  }
});

const ngInit = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-init');
    eval(value);
  }
});

const ngModel = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-model');
    const model = () => (el.value = $scope[value]);
    $scope.$watch('ngModel', model);
    el.oninput = () => {
      $scope[value] = el.value;
      $scope.$apply();
    };
    model();
  }

});

const ngRepeat = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-repeat').split(' ')[2];
    const parent = el.parentNode;

    const repeatNodes = compile => {
      deleteNodes(parent);
      const fragment = document.createDocumentFragment();

      const attrValue = typeof $scope[value] === 'string' ? $scope[value].split('') : $scope[value];
      attrValue.forEach(letter => {
        const newEl = el.cloneNode(true);
        newEl.removeAttribute('ng-repeat');
        newEl.setAttribute('repeated-element', true);
        newEl.style.display = '';
        newEl.textContent = el.textContent.replace(/{{(.)\w+}}/g, letter);

        if (typeof compile === 'function') {
          compile(newEl);
        }
        fragment.appendChild(newEl);
      });
      parent.appendChild(fragment);
    };
    repeatNodes();
    $scope.$watch(value, repeatNodes);
  }

});

const ngBind = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-bind');
    const bind = () => (el.textContent = $scope[value]);
    $scope.$watch(value, bind);
    bind();
  }

});

const ngShow = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-show');
    const show = () => (el.style.display = eval(value) ? '' : 'none');
    $scope.$watch(value, show);
    show();
  }
});

const ngHide = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-hide');
    const hide = () => (el.style.display = eval(value) ? 'none' : '');
    $scope.$watch(value, hide);
    hide();
  }
});

const ngUppercase = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-uppercase');
    const uppercase = () => (el.textContent = el.textContent.toUpperCase());
    $scope.$watch('value', uppercase);
    uppercase();
  }

});

const ngClick = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-click');
    el.addEventListener('click', e => $scope['itemClicked'](e));
  }
});

const ngMakeShort = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-make-short');
    const makeShort = () =>
      (el.textContent = `${el.textContent.slice(0, value)}...`);
    $scope.$watch(value, makeShort);
    makeShort();
  }

});

const ngLowercase = () => ({
  link: ($scope, el, attrs) => {
    const value = el.getAttribute('ng-uppercase');
    const lowerCase = () => (el.textContent = el.textContent.toLowerCase());
    $scope.$watch(value, lowerCase);
    lowerCase();
  }

});

export {
  ngIf,
  ngInit,
  ngModel,
  ngRepeat,
  ngBind,
  ngShow,
  ngHide,
  ngUppercase,
  ngClick,
  ngMakeShort,
  ngLowercase
};
