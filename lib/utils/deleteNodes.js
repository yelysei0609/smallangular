const deleteNodes = parent => {
  const nodes = parent.querySelectorAll('*');
  nodes.forEach(node => {
    if (node.getAttributeNames().includes('repeated-element')) {
      node.remove();
    }
  });
};

export default deleteNodes;