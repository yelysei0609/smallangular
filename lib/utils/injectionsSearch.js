/* eslint-disable no-eval */
export default function injectionsSearch($root, injections, services) {
  return injections.map(injection => {
    if (injection === '$scope') {
      return eval($root);
    }

    return eval(services[injection]);
  });
}

