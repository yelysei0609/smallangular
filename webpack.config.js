const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const publicPath = path.resolve(__dirname, 'public');


module.exports = {
  mode: 'development',
  devtool: 'inline-source-map',
  entry: {
    app: ['./lib/angular/angular.js',
      './src/index.js']
  },
  output: {
    path: publicPath,
    filename: '[name].js',
    libraryTarget: 'var',
    library: 'EntryPoint'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.html$/,
        exclude: /node_modules/,
        use: { loader: 'html-loader' }
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: ['public'] }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/index.html')
    }),
    new webpack.HotModuleReplacementPlugin()

  ],
  resolve: {
    extensions: ['.js', '.jsx', '.scss'],
    alias: {
      '@components': path.resolve(__dirname, 'src/components'),
      '@libUtils': path.resolve(__dirname, 'lib/angular/utils'),
      '@libDirectives': path.resolve(__dirname, 'lib/angular/directives')
    }
  }

};
